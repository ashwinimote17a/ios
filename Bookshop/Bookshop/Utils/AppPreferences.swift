//
//  AppPreferences.swift
//  Bookshop
//
//  Created by Mac HD on 19/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import Foundation


class AppPreferences {
    
    static let key_login_status = "login_status"
    static let key_user_name = "user_name"
    static let key_user_password = "user_password"
    
    class var isUserLoggedIn: Bool {
        if let status = UserDefaults.standard.value(forKey: key_login_status) as? Bool {
            return status
        } else {
            return false
        }
    }
    
 
    
    class func loginUser(username: String, password: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(username, forKey: key_user_name)
        userDefaults.setValue(password, forKey: key_user_password)
         userDefaults.setValue(true, forKey: key_login_status)
        userDefaults.synchronize()
    }
    
    class func logout() {
        let userDefaults = UserDefaults.standard
        userDefaults.setValue("", forKey: key_user_name)
        userDefaults.setValue("", forKey: key_user_password)
        userDefaults.setValue(false, forKey: key_login_status)
        userDefaults.synchronize()
    }
}

