//
//  Book.swift
//  Bookshop
//
//  Created by Mac HD on 20/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import Foundation
class Book {
    var bookid : Int!
    var category : String!
    var bookname : String!
    var  author : String!
    var thumbnail : String!
    var bookprice : Int!
    var bookdescription : String!
}
