//
//  Owner.swift
//  Bookshop
//
//  Created by Mac HD on 18/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import Foundation
class MyOwner
{
    var  firstname : String!;
    var lastname : String!;
    var email : String!;
    var address : String!;
    var city : String!;
    var state : String!;
    var pin  : String!;
    var username : String!;
    var password : String!
}
