//
//  Renter.swift
//  Bookshop
//
//  Created by Mac HD on 23/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import Foundation
class Renter
{
    var  firstname : String!;
    var lastname : String!;
    var email : String!;
    var address : String!;
    var city : String!;
    var state : String!;
    var pin  : String!;
    var username : String!;
    var password : String!
}
