//
//  BooKCollectionViewCell.swift
//  Bookshop
//
//  Created by Mac HD on 23/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit

class BooKCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelname: UILabel!
    
    @IBOutlet weak var labelbook: UILabel!
    @IBOutlet weak var labelinfo: UILabel!
    @IBOutlet weak var imageview: UIImageView!
}
