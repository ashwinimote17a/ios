//
//  AddBooklListViewController.swift
//  Bookshop
//
//  Created by Mac HD on 23/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit

class AddBooklListViewController: BaseViewController {

    @IBOutlet weak var editname: UITextField!
    
    @IBOutlet weak var editauthor: UITextField!
    
    @IBOutlet weak var editprice: UITextField!
    
    @IBOutlet weak var editdescription: UITextView!
    @IBOutlet weak var editcategory: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add Book"
        
    }

 
   
    @IBAction func onAdd() {

    
        let body = [
            "bookname": editname.text!,
            "author" : editauthor.text!,
            "bookprice": editprice.text!,
            "category" : editcategory.text!,
            "bookdescription":editdescription.text!,
    
        ]
        makeApiCall(api: "/book", onSuccess: {
            result in
            self.view.makeToast("successfully added books")
            self.navigationController?.popViewController(animated: true)
    
        },parameters:body)
    
    }
    
    
}
    



