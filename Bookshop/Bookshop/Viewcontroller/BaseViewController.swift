//
//  BaseViewController.swift
//  Bookshop
//
//  Created by Mac HD on 18/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit
import Alamofire

class BaseViewController: UIViewController {
    let url = "http://192.168.43.247:3000"

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    func launchVC(id: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: id)
    }
    
    func showWarning(message: String){
        view.makeToast(message)
    }
    
    
    func makeApiCall(api: String,onSuccess:@escaping ((_ data: Any?)->Void),onError:((_ data:Any?)->Void)? = nil , method: HTTPMethod = .post,parameters: Parameters? = nil){
        
        print("callingapi:\(api)")
        Alamofire
            .request(url + "\(api)", method: method, parameters:parameters)
                .responseJSON(completionHandler: { response in
                    let result = response.result.value as! [String: Any]
                     print(self.url)
                    print(result)
                    if result["status"] as! String == "success" {
                        print("data")
                        
                        onSuccess(result["data"])
                        } else {
                        if let onError = onError {
                            onError(result["data"])
                        }else {
                            self.showWarning(message: "api error")
                        }
                        
                    }
                })
    }

  

}
