//
//  RegisterViewController.swift
//  Bookshop
//
//  Created by Mac HD on 18/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit
import  Alamofire

class RegisterViewController: BaseViewController {
    
    
    @IBOutlet weak var editfirst: UITextField!
    
    @IBOutlet weak var editusername: UITextField!
    @IBOutlet weak var editpassword: UITextField!
    
    @IBOutlet weak var editaddress: UITextView!
    @IBOutlet weak var editlast: UITextField!
    
    @IBOutlet weak var editcontact: UITextField!
    @IBOutlet weak var editpin: UITextField!
    @IBOutlet weak var editstate: UITextField!
    @IBOutlet weak var editcity: UITextField!
  
    @IBOutlet weak var editemail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       title = "Register"
       
    }

    @IBAction func onregister() {
        let body = [
            "firstname":editfirst.text!,
            "lastname" :editlast.text!,
            "email":editemail.text!,
            "address": editaddress.text!,
            "city" : editcity.text!,
            "state": editstate.text!,
            "pin":editpin.text!,
            "contact":editcontact.text!,
            "ousername":editusername.text!,
            "opassword" : editpassword.text!
            ]
        makeApiCall(api: "/owner", onSuccess: {
            result in
            self.view.makeToast("successfully registered your account")
            self.navigationController?.popViewController(animated: true)
           
        },parameters:body)
       
       
       
      
        }
    
    @IBAction func onCancel() {
        dismiss(animated: true, completion:nil)
    }
}

