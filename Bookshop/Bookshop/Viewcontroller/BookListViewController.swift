//
//  BookListViewController.swift
//  Bookshop
//
//  Created by Mac HD on 20/07/19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit
import Kingfisher
import Toast_Swift

class BookListViewController: BaseViewController{
    
    @IBOutlet weak var collectionview: UICollectionView!
    
    
    var books : [Book] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "BOOKS"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAdd))
        
      
    }
    @objc func onAdd()
    {
        let vc = launchVC(id : "AddBooklListViewController")
        navigationController?.pushViewController(vc,animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        loadBooks()
    }

      func loadBooks()
      {        makeApiCall(api: "/book", onSuccess: { response in
            self.books.removeAll()
            let array = response as! [[String :Any]]
            for  obj in array {
                let book = Book()
                book.bookid = obj["bookid"] as? Int
                book.bookname = obj["bookname"] as? String
                book.author = obj["author"] as? String
                book.bookprice = obj["bookprice"] as? Int
                book.thumbnail = obj["thumbnail"] as? String
                book.bookdescription = obj["bookdescription"] as? String
                self.books.append(book)
            }
            self.collectionview.reloadData()
            print("hello")
            
        },method: .get)
    }
  

}
extension BookListViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return books.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BooKCollectionViewCell", for: indexPath) as! BooKCollectionViewCell
        let book = books[indexPath.row]
        
        cell.labelbook.text = book.bookname
        cell.labelinfo.text = "\(book.bookprice!)"
        cell.labelname.text = book.author
        
        let imageUrl = URL(string: url + "/\(book.thumbnail!)")
    
        cell.imageview.kf.setImage(with:imageUrl)
        return cell
    }
    }

extension BookListViewController:UICollectionViewDelegate{
    
}
extension BookListViewController :UICollectionViewDelegateFlowLayout{
    func collectionView(_collectionView:UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width / 3) - 10
        return CGSize(width: width, height: 200)
    }
}
